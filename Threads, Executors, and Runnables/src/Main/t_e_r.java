
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class t_e_r {
    static final int NUMBER_THREADS = 50;

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        Runnable task = () -> {
            String threadname = Thread.currentThread().getName();
            System.out.println("Hello " + threadname);
        };

        Thread thread = new Thread(task);
        thread.start();

        for (int i = 0; i < arr1.length; i++) {
            System.out.println(arr1[i]);
        }

        System.out.println("");
        System.out.println("");
        System.out.println("");

        Runnable runner1 = () -> {
            System.out.println("Which runner will win the race?");
        };

        Runnable runner2 = () -> {
            System.out.println("Will they execute in the same order?");
        };

        Runnable runner3 = () -> {
            System.out.println("How will this end?");
        };

        ExecutorService executor = Executors.newFixedThreadPool(3);

        executor.submit(runner1);
        executor.submit(runner2);
        executor.submit(runner3);

        Counter counter = new Counter();
        AtomCounter aCounter = new AtomCounter();

        System.out.println("\n");
        System.out.println("\n");

        System.out.println("Initial Counter = " + counter.get());
        System.out.println("Initial Atom Counter = " + aCounter.get());

        UpdateThread[] threads = new UpdateThread[NUMBER_THREADS];
        AtomUpdate[] atomThreads = new AtomUpdate[NUMBER_THREADS];

        for (int i = 0; i < NUMBER_THREADS; i++) {
            threads[i] = new UpdateThread(counter);
            threads[i].start();
        }

        for (int i = 0; i < NUMBER_THREADS; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException ex) { ex.printStackTrace(); }
        }

        for (int i = 0; i < NUMBER_THREADS; i++) {
            atomThreads[i] = new AtomUpdate(aCounter);
            atomThreads[i].start();
        }

        for (int i = 0; i < NUMBER_THREADS; i++) {
            try {
                atomThreads[i].join();
            } catch (InterruptedException ex) { ex.printStackTrace(); }
        }

        System.out.println("\n");
        System.out.println("Final Counter = " + counter.get());
        System.out.println("Final Counter = " + aCounter.get());

        if (counter.get() >= 50) {
            System.out.println("Non-Atomic launch!");
        }

        if (aCounter.get() >= 50) {
            System.out.println("Atomic launch!");
        }

    }
}